# Database management systems

### Sargability

__Sargability__ - attribute of an optimized query (in terms of database engine execution via __indexes__ usage).
The term is derived from a contraction of __Search ARGument ABLE__.

__SARGable__ query includes:
* consuming less system resources 
* speeding up query performance 
* using indexes more effectively 

Indexes can be used by DBMS engine in two different ways:
1. An index scan is when DBMS engine reads all the data in the index pages. The cost of an index scan is very expensive.
2. An index seek is when DBMS engine reads only matching data in the index pages. This method is more efficient for query performance because it will reduce IO and time consumption. 

Let's look at example:

```
CREATE INDEX ON person (first_name);
...
SELECT first_name FROM person WERE LEFT(first_name, 1) = 'A';
```

This is not sargable because `first_name` is embedded in a function. 
If any indexes were available on `first_name`, they could not be used. 
In addition, `LEFT()` would be called on every record in `person` table. 

__Summary:__
The PostgreSQL query optimizer cannot find the result of the [left function](https://www.postgresql.org/docs/9.1/functions-string.html) values in the index pages. 
For this reason, the query optimizer chooses a sequnce scan and it needs to read the whole table. 

Example of __SARGable__ version will be:

```
SELECT first_name FROM person WERE first_name = 'Adam';
```

### CTE (Common Table Expression)
[CTE](https://postgrespro.com/docs/postgrespro/9.5/queries-with) is SQL "`WITH`" clause, more commonly used for simplifying very complex queries (subqueries).
In fact, this is creating temporary tables, but only performed for one request, not for the session. They can be accessed within this request. 
Such a request is well readable, understandable, and easy to modify if required.

```
WITH CTE_name (column_name_list) AS (
CTE_SQL_statement_definition
)
statement;
```


* CTE_name: We define the name to be given to the common table expression. The names of the columns follow the common table expression. The column name list is an optional list.
* CTE_SQL_statement_definition: We define the SQL statement, which returns the result set inside the body of the WITH clause. The CTE_SQL_statement_definition becomes the names of the column list of the common table expression if we have not defined the column names after the names of the common table expression.
* statement: This can be used as a view or table in the statement, which can be a SELECT, INSERT, UPDATE, or deletes SQL statements.


As example:

```
WITH avg_salary AS (
    SELECT avg(salary)
    FROM employees
)
SELECT * FROM avg_salary;
```

### Recursive queries
What are recursive queries good for? — In general recursive queries come in handy when working with self-referential data or graph/tree-like data structures. Just a few examples of these use cases are:

Self-referential data:

    * Manager -> Subordinate (employee) relationship
    * Category -> Subcategory -> Product relationship
    * Graphs — Flight (Plane Travel) map

Trees:

    * Any taxonomy system — books, animals, genetics…
    * Links between articles — for example on Wikipedia
    * Comment section — for example threads on Reddit

For any of these examples, it would require you to build temporary tables or use cursors to actually get useful data from such datasets.

Recursive query based on CTE:

```
WITH RECURSIVE recursive_query (
    anchor_cte
    UNION
    query_iteration_cte
) query_statement;
```

Where `anchor_cte` - initial part of recursive query, which executes only once,  `query_iteration_cte` - recursive query part, `query_statement` - final selection.

>Strictly speaking, this process is iteration not recursion, but RECURSIVE is the terminology chosen by the SQL standards committee. 


Fibonacci numbers can be selected via recursive SQL query:

```
WITH RECURSIVE fibonacci(a, b) AS (
    VALUES (0, 1)
    UNION
    SELECT b, a + b 
    FROM fibonacci
    WHERE a + b < 100
)
SELECT a + b FROM fibonacci;
```

The general form of a recursive WITH query is always a non-recursive term, then UNION (or UNION ALL), then a recursive term, where only the recursive term can contain a reference to the query's own output. Such a query is executed as follows: 

1. Evaluate the non-recursive term. For UNION (but not UNION ALL), discard duplicate rows. Include all remaining rows in the result of the recursive query, and also place them in a temporary working table.

2. So long as the working table is not empty, repeat these steps:

* Evaluate the recursive term, substituting the current contents of the working table for the recursive self-reference. For UNION (but not UNION ALL), discard duplicate rows and rows that duplicate any previous result row. Include all remaining rows in the result of the recursive query, and also place them in a temporary intermediate table.

* Replace the contents of the working table with the contents of the intermediate table, then empty the intermediate table. 

Real-world examples:

Let's imagine, that we have table for geo-locations

```
CREATE TABLE geo
(
    id        int not null primary key,
    parent_id int references geo (id),
    name      varchar(1000)
);

INSERT INTO geo (id, parent_id, name)
VALUES (1, null, 'Earth Planet'),
       (2, 1, 'Eurasia Continent'),
       (3, 1, 'North America Continent'),
       (4, 2, 'Europe'),
       (5, 4, 'Russia'),
       (6, 4, 'Germany'),
       (7, 5, 'Moscov'),
       (8, 5, 'Saint-P'),
       (9, 6, 'Berlin');

```

What if we need to find all stuff, which somehow (not __directly__) related to `Europe`?

```
SELECT id, parent_id, name FROM geo WHERE parent_id = 4;
```

Such query will return only 2 rows: `Russia` and `Germany`, but looks like we missed `Moscov` (`Russia -> Europe`), `Saint-P` (`Russia -> Europe`) and `Berlin` (`Germany -> Europe`). In such case, when data is graph oriented, we can use recursive clause:

```
WITH RECURSIVE geolocations_with_children AS (
    SELECT id, parent_id, name FROM geo WHERE parent_id = 4
    UNION
    SELECT geo.id, geo.parent_id, geo.name FROM geo
        JOIN geolocations_with_children ON geo.parent_id = geolocations_with_children.id
) SELECT name FROM geolocations_with_children;
```

`JOIN geolocations_with_children` used here, because via `WITH` clause we have temporal table.

### Graph Data model
If your application has mostly one-to-many relationships (tree-structured data) or no relationships between records, the documentmodel is appropriate.
But what if many-to-many relationships are very common in your data? The relaional model can handle simple cases of many-to-many relationships, 
but as the connections within your data become more complex, it becomes more natural to start modeling your data as a graph.
A graph consists of two kinds of objects: vertices (also known as nodes or entities) and edges (also known as relationships or arcs). 
Many kinds of data can be modeled as agraph. Typical examples include:

* Social graphs

Vertices are people, and edges indicate which people know each other.
The web graph

Vertices are web pages, and edges indicate HTML links to other pages.

* Road or rail networks

Vertices are junctions, and edges represent the roads or railway lines between
them.

Well-known algorithms can operate on these graphs: for example, car navigation sys‐
tems search for the shortest path between two points in a road network, and
PageRank can be used on the web graph to determine the popularity of a web page
and thus its ranking in search results.

Graph model DDL:

```
CREATE TABLE vertices
(
    vertex_id  integer PRIMARY KEY,
    properties json
);

CREATE TABLE edges
(
    edge_id     integer PRIMARY KEY,
    tail_vertex integer REFERENCES vertices (vertex_id),
    head_vertex integer REFERENCES vertices (vertex_id),
    label       text,
    properties  json
);

CREATE INDEX edges_tails ON edges (tail_vertex);
CREATE INDEX edges_heads ON edges (head_vertex);
```

And test data for defining person, who was born in USA and lives in Paris:

```
INSERT INTO vertices VALUES
(1, '{ "name": "North America", "type": "continent" }'),
(2, '{ "name": "Europe", "type": "continent" }'),
(3, '{ "name": "Paris", "type": "city" }'),
(4, '{ "name": "United States", "type": "country" }'),
(5, '{ "name": "Idaho", "type": "state" }'),
(6, '{ "name": "Adam" }');

INSERT INTO edges VALUES
(1, 5, 4, 'within', '{}'::jsonb),    -- Idaho -[within]-> USA
(2, 3, 2, 'within', '{}'::jsonb),    -- Paris -[within]-> Europe
(3, 6, 5, 'born_in', '{}'::jsonb),   -- Adam -[born in]-> Idaho (USA)
(4, 6, 3, 'lives_in', '{}'::jsonb);  -- Adam -[lives in]-> Paris
```

Person Adam was born in Idaho state in USA, but now lives in Paris (Europe). 
How can such graph-based data queried via condition: __person was born in USA and now lives in Europe__?
Something like this:

```
WITH RECURSIVE

-- in_usa is the set of vertex IDs of all locations within the United States
in_usa(vertex_id) AS (
    SELECT vertex_id
    FROM vertices
    WHERE properties ->> 'name' = 'United States'
    UNION
    SELECT edges.tail_vertex
    FROM edges
             JOIN in_usa ON edges.head_vertex = in_usa.vertex_id
    WHERE edges.label = 'within'
),

-- in_europe is the set of vertex IDs of all locations within Europe
in_europe(vertex_id) AS (
    SELECT vertex_id
    FROM vertices
    WHERE properties ->> 'name' = 'Europe'
    UNION
    SELECT edges.tail_vertex
    FROM edges
             JOIN in_europe ON edges.head_vertex = in_europe.vertex_id
    WHERE edges.label = 'within'
),

-- born_in_usa is the set of vertex IDs of all people born in the US
born_in_usa(vertex_id) AS (
    SELECT edges.tail_vertex
    FROM edges
             JOIN in_usa ON edges.head_vertex = in_usa.vertex_id
    WHERE edges.label = 'born_in'
),

-- lives_in_europe is the set of vertex IDs of all people living in Europe
lives_in_europe(vertex_id) AS (
    SELECT edges.tail_vertex
    FROM edges
             JOIN in_europe ON edges.head_vertex = in_europe.vertex_id
    WHERE edges.label = 'lives_in'
)

SELECT vertices.properties ->> 'name' as result
FROM vertices
-- join to find those people who were both born in the US *and* live in Europe
         JOIN born_in_usa ON vertices.vertex_id = born_in_usa.vertex_id
         JOIN lives_in_europe ON vertices.vertex_id = lives_in_europe.vertex_id;
```


https://research.cs.wisc.edu/coral/minibase/project.html

### Storage 
There are two types of storrage: volatile and non-volatile. Volatile storage need energy for work and don't store data for long time, non-volatile storage persists data, that's because after restart data will not be losed.

<a href="https://ibb.co/8gKcs7D"><img src="https://i.ibb.co/qYy5jrM/image.png" alt="image" border="0"></a>

### Disk and IO in low-level (RAID)
[link](http://www-db.deis.unibo.it/courses/TBD/Lezioni/01%20-%20PhysycalDBMS.pdf)

<a href="https://ibb.co/7bw8Gcd"><img src="https://i.ibb.co/4J69jyn/image.png" alt="image" border="0"></a>

### Disk-Oriented DBMSs
The primary storage location of the database is on [non-volatile storage](https://en.wikipedia.org/wiki/Non-volatile_memory) (flash SSD, DRAM and so on). The database is organized as a set of __fixed-length pages__ (aka blocks).

_But work with disk storage is very expensive operation for IO, that's why disk-oriented [DBMS systems introduce in-memory buffer](https://documentation.progress.com/output/ua/OpenEdge_latest/index.html#page/dmadm/database-buffers.html)._

The system uses an __in-memory buffer__ pool to [cache](https://www.youtube.com/watch?v=x2vegjeJICk) pages fetched from disk. It's job is to manage the movement of those pages back and forth between disk and memory.

When a query accesses a page, the __DBMS__ checks to see if that page is already in memory:
* If it's not, then the DBMS must retrieve it from disk and copy it into a frame in it's buffer pool
* If there are no free frames, then find a page to evict
* If the page being evicted is dirty, then the DBMS must write it back to disk

Once the page is in memory, the DBMS translates any on-disk addresses to their in-memory addresses.

<a href="https://ibb.co/tCQw6Zp"><img src="https://i.ibb.co/Sv5DgQw/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/yXRHKmb"><img src="https://i.ibb.co/G0JzGfb/image.png" alt="image" border="0"></a>


* `Page table` - hashtable, which have `page ID + slot number` as key and link to buffer as value.
* `Buffer pool` contains pages of data in RAM.  Buffer pool can have count of usages, dirty data flag, pin count.
* `Database on-disk` - disk storage.

<a href="https://ibb.co/1TpSG24"><img src="https://i.ibb.co/7KT9ty0/image.png" alt="image" border="0"></a>

[How it works in postgre SQL](http://www.interdb.jp/pg/pgsql08.html) and also [here](https://habr.com/ru/company/postgrespro/blog/458186/).

### Storage organisation
Inside DBMS data (tables, indexes and e.t.c) might be organised in several ways.
Database pages might be organized as:
1. Heap file
2. Sequential/Sorted file
3. Hashing file

__Heap file__ - unordered collection of pages where tuples stored in order of insertion. 
Heap file need special metadata to keep track of what pages already exists and which ones have free space.
Heap file might be represented as _Linked List_ or _Page Directory_.

Pros:
* Inserting a new record is very efficient
* Reuse delocated space

Cons:
* Searching for a record requires linear search

__Heap File via Linked List implementation__

Heap File linked list has two pointers: 
1. Head for free pages (used when page delocated and rows removed from the disk)
2. Head for data pages

Such solution maintaining a doubly linked list of pages with free space and a doubly linked list of full pages.
Together, these lists contain all pages in the heap file.

<a href="https://ibb.co/28vXLTJ"><img src="https://i.ibb.co/Xy7mdqH/image.png" alt="image" border="0"></a>

__Heap File via Page Directory implementation__

The DBMS maintains special pages that tracks the location of data pages in the database file. 
The directory also records the number of free slots per page.
The DBMS has to make sure that directory pages are in sync with the data pages.

<a href="https://ibb.co/HKCvbst"><img src="https://i.ibb.co/2FS2CQv/image.png" alt="image" border="0"></a>

### Disk space manager
The lowest level of software in the DBMS architecture called the __[disk space manager](http://dbmsfortech.blogspot.com/2016/05/disk-space-management.html)__, manages space on disk. Abstractly, the disk space manager supports the concept of a pageas a unit of data, and provides commands to allocateor/deallocate a page and read or write a page.  The size of a page is chosen to be the size of a disk block and pages are stored as disk blocks so that reading or writing a page can be done in one disk I/O.

First of all, DBMS space managers perform next operations:
* allocate page
* dellocate page
* get content of page by id
* write content to new page

Basic interface presented [here](https://gitlab.com/aleksandr.tamazian/database/-/blob/master/dbms/src/core/space/manager/SpaceManager.kt).

_Many database systems __do not rely on the OS file system__ and instead do their owndisk  management, either from scratch or by  extending  OS facilities. The  reasonsare practical as well as technical. One practical reason is that a DBMS vendor whowishes to support several OS platforms cannot assume features speci c to any OS,for portability, and would therefore try to make the DBMS code as self-contained aspossible.  A technical reason is that on a 32-bit system, the largest file size is 4 GB,whereas a DBMS may want to access a single  le larger than that. A related problem isthat typical OS  les cannot span disk devices, which is often desirable or even necessaryin a DBMS_.

### Page
How to organizing data storage inside file? 

<a href="https://ibb.co/JRTgscX"><img src="https://i.ibb.co/rGhNwQn/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/gjn08VH"><img src="https://i.ibb.co/bWf4VJq/image.png" alt="image" border="0"></a>

In such case, it's more convinient to store data __inside special pages (aka disk blocks)__. Page have some meta-information like:
* Id of current page in the db
* A timestamp indicating when the page was last modified
* he ID of the relation of the records stored in the page, etc.

The abstraction of a page is provided by the [Page class](https://gitlab.com/aleksandr.tamazian/database/-/blob/master/dbms/src/core/storage/page/Page.kt). All higher level applications use this `Page` class. Higher layers impose their own structure on pages simply by casting page pointers to their own record types. The data part of a page is guaranteed to start at the beginning of the block.

### Log-structured storage (SSTables & LMS Tree)

#### SST (Sorted String Tables)
Let's imagine that we have a log file of all operations related to youtube videos. 
Data stored as key-value pair, where key - author and value - count of views.

```
Vasya:10, Igor:500, David:434, ... John:1000
```

Such a data log is immutable. Write operations are really simple and fast. 
But they have one drawback. Reading will be long if we want to search for a specific value. 
Also, it will be harder to update specific value in such case, because more work might be done while fetching.


Each  log-structured  storage  segment  is  a  sequence  of  key-value  pairs.
These pairs appear in the order that they were written, and values later in the log take
precedence over values for the same key earlier in the log. Apart from that, the order
of key-value pairs in the file does not matter.
Now we can make a simple change to the format of our segment files: we require that
the  sequence  of  key-value  pairs  is  sorted  by  key.  At  first  glance,  that  requirement
seems to break our ability to use sequential writes, but we’ll get to that in a moment.
We  call  this  format  Sorted  String  Table,  or  SSTable  for  short

So, it's more convenient to split such log-based data into segments and compact by merging operations:

<a href="https://ibb.co/X4mvW3b"><img src="https://i.ibb.co/pPC91Rx/image.png" alt="image" border="0"></a>

What  if  the  same  key  appears  in  several  input  segments?  
Remember  that  each segment  contains  all  the  values  written  to  the  database  during  some  period  of time.  
This  means  that  all  the  values  in  one  input  segment  must  be  more  recent than all the values in the other segment (assuming that we always merge adjacent segments). When multiple segments contain the same key, we can keep the value from the most recent segment and discard the values in older segments.

Then, all data sorted by keys, which helps to use indexes more effective. 
You still need an in-memory index to tell you the offsets for some of the keys, but it  can  be  sparse:  one  key  for  every  few  kilobytes  of  segment  file  is  sufficient,
because a few kilobytes can be scanned very quickly

### Buffer manager
Buffer manager is the software layer that is responsible for bringing pages from disk to main memory as needed. The buffer manager manages the available main memory by partitioning it into a collection of pages, which we collectively refer to as the __buffer pool__. The main memory pages in the buffer pool are called __frames__; it is convenient to think of themas slots that can hold a page (that usually resides on disk or other secondary storage media). Higher levels of the DBMS code can be written without worrying about whether data pages are in memory or not; they ask the buffer manager for the page, and it is brought into a frame in the buffer pool if it is not already there. Of course, the higher-level code that requests a page must also release the page when it is no longer needed, by informing the buffer manager, so that the frame containing the page can be reused. The higher-level code must also inform the buffer manager if it modifes the requested page; the buffer manager then makes sure that the change is propagated to the copy of the page on disk.

<a href="https://ibb.co/vD4pXPL"><img src="https://i.ibb.co/1bG1zqR/image.png" alt="image" border="0"></a>

#### Buffer manager page pinning
In addition to the buffer pool itself, the buffer manager maintains some bookkeeping information, and two variables for each frame in the pool: `pin count` and `dirty`. 
The number of times that the page currently in a given frame has been requested but not released—the number of current users of the page—is recorded in the `pin count` variable for that frame. The boolean variable dirty indicates whether the page has been modified since it was brought into the buffer pool from disk.
Initially, the `pin count` for every frame is set to 0, and the dirty bits are turned off.
When a page is requested the buffer manager does the following:

1. Check buffer pool, if page exists - return it
2. If page not exists in buffer pool, then chose a page for replacement (LRU, Clock replacement strategies)
3. If chosen page is dirty - write it to disk

In general, idea of page fetching presented in next example:
```
class DiskSpaceManager {
    ...
    public Page loadPage(DBFile dbFile, int pageNo, boolean create) throws IOException {
        // Try to retrieve from the buffer manager.
        DBPage dbPage = bufferManager.getPage(dbFile, pageNo);
        if (dbPage == null) {
            // Buffer manager didn't have it.  Read the page directly from
            // the file, then add it to the buffer manager.
            dbPage = new DBPage(bufferManager, dbFile, pageNo);
            try {
                fileManager.loadPage(dbFile, pageNo, dbPage.getPageData(), create);
                bufferManager.addPage(dbPage);
            }
            catch (IOException e) {
                // Make sure to release the DBPage's buffer, or else we will
                // have a resource leak...
                dbPage.invalidate();
                throw e;
            }
        }

        return dbPage;
    }

}

class BufferManager {
    ...
     public DBPage getPage(DBFile dbFile, int pageNo) {
        DBPage dbPage = cachedPages.get(new CachedPageInfo(dbFile, pageNo));

        logger.debug(String.format(
            "Requested page [%s,%d] is%s in page-cache.",
            dbFile, pageNo, (dbPage != null ? "" : " NOT")));

        if (dbPage != null) {
            // Make sure this page is pinned by the session so that we don't
            // flush it until the session is done with it.
            dbPage.pin();
        }

        return dbPage;
    }
}
```

### Isolation & transactions
__Transaction__ - set of operations, which produced by application and move database from one consistent state to another in terms of transaction.
Transaction executes as atomic operation in database terms.

#### Isolation levels

<a href="https://ibb.co/ZKCBpWd"><img src="https://i.ibb.co/7VFnd2v/image.png" alt="image" border="0"></a>

### MVCC
