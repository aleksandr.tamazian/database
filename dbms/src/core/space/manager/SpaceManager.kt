package core.space.manager

import core.constants.PAGE_SIZE
import core.storage.page.Page
import core.storage.page.PageId
import java.io.IOException

/**
 * Interface for space management of database.
 * Describe a contract for database space manager.
 *
 * @link https://research.cs.wisc.edu/coral/minibase/spaceMgr/dsm.html
 * @author Aleksandr Tamazian
 */
interface SpaceManager {

    /**
     * Open database with the given name.
     *
     * @param name Name of database
     *
     * @exception IOException I/O errors.
     */
    @Throws(IOException::class)
    fun openDB(name: String)

    /**
     * Open database with the given name and with the specified number of pages (and with page size).
     *
     * @param name Name of database
     * @param numPages number of pages in database
     * @param pageSize size of each page (default size is page size constant)
     *
     * @exception IOException I/O errors.
     */
    @Throws(IOException::class)
    fun openDB(name: String, numPages: Int, pageSize: Int = PAGE_SIZE)

    /**
     * Destroy the whole database, removing the file that stores it.
     *
     * @exception IOException I/O errors.
     */
    @Throws(IOException::class)
    fun destroyDB()

    /**
     * Read the contents of the specified page into a Page object.
     *
     * @param pageId pageId which will be read
     * @param page page object which holds the contents of page
     *
     * @exception InvalidPageNumberException invalid page number
     * @exception FileIOException file I/O error
     * @exception IOException I/O errors
     */
    @Throws(IOException::class)
    fun readPage(pageId: PageId, page: Page)

    /**
     * Write the contents in a page object to the specified page.
     *
     * @param pageId pageId will be wrote to disk
     * @param page the page object will be wrote to disk
     *
     * @exception InvalidPageNumberException invalid page number
     * @exception FileIOException file I/O error
     * @exception IOException I/O errors
     */
    @Throws(IOException::class)
    fun writePage(pageId: PageId, page: Page)

    /**
     * Allocate a set of pages where the run size is taken to be 1 by default.
     * Gives back the page number of the first page of the allocated run.
     *
     * @param startPageNum page number to start with
     * @param runSize the number of page need allocated (default value is 1)
     *
     * @exception OutOfSpaceException database is full
     * @exception InvalidRunSizeException invalid run size
     * @exception InvalidPageNumberException invalid page number
     * @exception FileIOException DB file I/O errors
     * @exception IOException I/O errors
     * @exception DiskMgrException error caused by other layers
     */
    @Throws(IOException::class)
    fun allocatePage(startPageNum: PageId, runSize: Int = 1)

    /**
     * Deallocate a set of pages starting at the specified page number and
     * a run size can be specified.
     *
     * @param startPageNum the start pageId to be deallocate
     * @param runSize the number of pages to be deallocated (default value is 1)
     *
     * @exception InvalidRunSizeException invalid run size
     * @exception InvalidPageNumberException invalid page number
     * @exception FileIOException file I/O error
     * @exception IOException I/O errors
     * @exception DiskMgrException error caused by other layers
     */
    @Throws(IOException::class)
    fun deallocatePage(startPageNum: PageId, runSize: Int = 1)

    /**
     * Adds a file entry to the header page(s).
     *
     * @param fileName file entry name
     * @param startPageNum the start page number of the file entry
     *
     * @exception FileNameTooLongException invalid file name (too long)
     * @exception InvalidPageNumberException invalid page number
     * @exception InvalidRunSizeException invalid DB run size
     * @exception DuplicateEntryException entry for DB is not unique
     * @exception OutOfSpaceException database is full
     * @exception FileIOException file I/O error
     * @exception IOException I/O errors
     * @exception DiskMgrException error caused by other layers
     */
    @Throws(IOException::class)
    fun addFileEntry(fileName: String, startPageNum: PageId)

    /**
     * Delete the entry corresponding to a file from the header page(s).
     *
     * @param fileName file entry name
     *
     * @exception FileEntryNotFoundException file does not exist
     * @exception FileIOException file I/O error
     * @exception IOException I/O errors
     * @exception InvalidPageNumberException invalid page number
     * @exception DiskMgrException error caused by other layers
     */
    @Throws(IOException::class)
    fun deleteFileEntry(fileName: String)

    /**
     * Get the entry corresponding to the given file.
     *
     * @param name file entry name
     * @param startPage the start page number of the file entry
     *
     * @exception IOException I/O errors
     * @exception FileIOException file I/O error
     * @exception InvalidPageNumberException invalid page number
     * @exception DiskMgrException error caused by other layers
     */
    @Throws(IOException::class)
    fun getFileEntry(name: String, startPage: PageId): Page
}