package core.storage.page

data class PageId(
        val pid: Int = 0
) {
    /**
     * Write the pid into a specified bytearray at offset
     * @param data the specified bytearray
     * @param offset the offset of bytearray to write the pid
     * @exception  java.io.IOException I/O errors
     */
    @Throws(java.io.IOException::class)
    fun writeToByteArray(data: ByteArray, offset: Int) {
        //Convert.setIntValue(pid, offset, ary)
    }
}