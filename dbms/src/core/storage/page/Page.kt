package core.storage.page

import core.constants.MAX_SPACE

/**
 * Abstraction for disc blocks mapping to logical pages.
 *
 * @author Aleksandr Tamazian
 */
class Page {

    private var data: ByteArray

    constructor() {
        data = ByteArray(MAX_SPACE)
    }

    constructor(data: ByteArray) {
        this.data = data
    }

    /**
     * Return the data byte array.
     * @return the byte array of the page
     */
    fun getPage(): ByteArray {
        return data
    }

    /**
     * Set the page with the given byte array.
     * @param data a byte array of page size
     */
    fun setPage(data: ByteArray) {
        this.data = data
    }

}