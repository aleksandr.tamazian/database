package core.constants

/**
 * Constants for dbms usage.
 *
 * @author Aleksandr Tamazian
 */

const val PAGE_SIZE = 1 // Default size of one page
const val MAX_SPACE = 1024 // In frames
